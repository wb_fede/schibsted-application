# Schibsted Application

El proyecto consiste en implementar una Web Application usando el lenguaje C#.

Esta aplicación tiene 3 páginas privadas diferentes y un formulario de inicio de sesión. Para acceder a cualquiera de estas páginas privadas, el usuario debe iniciar una sesión a través del formulario de inicio de sesión y debe tener el rol correcto para poder acceder a la página.

La aplicación también tiene una REST API que expone el recurso "Usuario". La creación, eliminación y modificación de usuarios y sus permisos se realizan a través de esta API.

## Getting Started

### Prerequisites

	- Visual Studio 2017
	- Postman

	Los puertos preconfigurados son
	- Client 49623
	- API 51553

### Installing

Para ejecutar es necesario clonar el repositorio y abrirlo con el Visual Studio. Luego presionando F5 se podrá visualizar el sitio.

Al hacer esto se ejecutarán 2 soluciones: La api Rest y el Sitio web.

## Running the tests

### API Rest

La Api rest contiene los siguientes métodos:

	- Get User (GET): obtiene un JSON con todos los usuarios disponibles

	Header: Authentication - Content Type

	- Get Especific User (GET): obtiene los Roles de un usuario. El usuario consultado debe ser el mismo que se utiliza en la authentication.

	Header: Authentication - Content Type
	Parametro en URL (/api/user/page1): usuario

	- New User (POST): creación de un nuevo usuario
	
	Header: Authentication - Content Type
	Body: RAW - JSON

	- Edit User (PUT): editar datos de un usuario

	Header: Authentication - Content Type
	Body: RAW - JSON

	- Delete Usuario (DELETE): eliminación de un usuario.

	Header: Authentication - Content Type
	Body: RAW - JSON

La API cuenta con Basic Authentication, el cuál requiere de un usuario y contraseña. Sólo los usuarios con roles Admin podrán utilizar la API, excepto el método "Get Especific User", el cuál es utilizado por la APP y permite ver los roles del usuario en sesión.

En el siguiente link se puede encontrar una colección de Postman para probar la API

https://www.getpostman.com/collections/c4c7a972bab98dcc6ddc


### APP Client

La aplicación Web contiene una página inicial con un botón de Login. Además 3 páginas privadas que sólo pueden ser accedidas mediante una sesión establecida y con el rol correspondiente.

Los roles actuales son:

	- Admin
	- Page1
	- Page2
	- Page3

Al iniciar sesión el usuario puede visualizar todas las páginas que tiene disponibles por su rol.

El Sistema cuenta con 5 usuarios por defecto:

	- Administrador
		- Username = admin
		- Password = Admin
		- Roles = Admin
	- Page1
		- Username = page1
		- Password = page1
		- Roles = Page1
	- Page2
		- Username = page2
		- Password = page2
		- Roles = Page2
	- Page3
		- Username = Page3
		- Password = Page3
		- Roles = Page3
	- PageAll
		- Username = pageAll
		- Password = pageAll
		- Roles = ['Page1',Page2','Page3']

## Unit Test

El proyecto cuenta con un módulo con 6 Unit test

	-TestLoginWithoutReturn: Revisa que el método Login, sin recibir el parámetro returnUrl, funcione correctamente

	-TestLoginWithReturn: Revisa que el método Login, recibiendo el parámetro Page 1, defina el ViewData correctamente

	-TestPageIndex: Revisa que el método Index funcione correctamente

	-TestPagePage1: Revisa que el método Page1 funcione correctamente

	-TestPagePage2: Revisa que el método Page2 funcione correctamente

	-TestPagePage3: Revisa que el método Page3 funcione correctamente

Todos estos Test son muy básicos y sirven para detectar excepciones. El resto de las pruebas se realizaron mediante la colección de postman y en el sitio web.

## Built With

	- Microsoft.NETCore.App 2.1.4
	- Microsoft.AspNetCore.App 2.1.4
	- Microsoft.AspNetCore.All 2.1.4


## Authors

Devoto Federico 

