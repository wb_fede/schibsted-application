﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using ClientSite.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        /************ Login Method  ****************/
        [TestMethod]
        public void TestLoginWithoutReturn()
        {
            // test login controller
            var controller = new AccountController();

            ViewResult result = controller.Login(null) as ViewResult;

            // Assert
            Assert.AreEqual(result.ViewName,"");

        }

        [TestMethod]
        public void TestLoginWithReturn()
        {
            // test login controller
            var controller = new AccountController();

            ViewResult result = controller.Login("Page1") as ViewResult;

            // Assert
            Assert.AreEqual(result.ViewData["ReturnUrl"], "Page1");

        }

        /************ Index Method  ****************/
        [TestMethod]
        public void TestPageIndex()
        {
            // test login controller
            var controller = new PagesController();

            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.AreEqual(result, null);
        }

        [TestMethod]
        public void TestPagePage1()
        {
            // test login controller
            var controller = new PagesController();

            ViewResult result = controller.Page1() as ViewResult;

            // Assert
            Assert.AreEqual(result, null);
        }

        [TestMethod]
        public void TestPagePage2()
        {
            // test login controller
            var controller = new PagesController();

            ViewResult result = controller.Page2() as ViewResult;

            // Assert
            Assert.AreEqual(result, null);
        }

        [TestMethod]
        public void TestPagePage3()
        {
            // test login controller
            var controller = new PagesController();

            ViewResult result = controller.Page3() as ViewResult;

            // Assert
            Assert.AreEqual(result, null);
        }
    }
}
