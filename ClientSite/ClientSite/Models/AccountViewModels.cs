﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClientSite.Models
{

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Usuario")]
        [MinLength(5)]
        [StringLength(30)]
        public string Usuario { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MinLength(5)]
        [StringLength(10)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }
    }


}
