﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientSite.Models
{
    public class ResponseModel
    {
        public int StatusCode { get; set; }
        public string[] Roles { get; set; }

    }
}