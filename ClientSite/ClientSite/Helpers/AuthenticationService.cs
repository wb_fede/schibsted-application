﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using ClientSite.Models;

namespace ClientSite.Helpers
{
    public class AuthenticationService : IRequiresSessionState
    {
        private string _uri = "http://localhost:51553/api/user";
        private ResponseModel _response = new ResponseModel();
        private HttpContext context = HttpContext.Current;

        public ResponseModel CheckAuthentication(string username = null, string password = null)
        {
            HttpClientHandler handler = new HttpClientHandler();
            HttpClient clientHttp = new HttpClient(handler);

            // BASIC AUTH
            string user;
            string uri;
            object session;
            user = username + ":" + password;
            //define uri
            uri = _uri + "/" + username;
         
            //encode basic auth
            var byteArray = Encoding.ASCII.GetBytes(user);
            string  basicAuth = Convert.ToBase64String(byteArray);
            
            clientHttp.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", basicAuth);

            // HTTP Request
            try
            {
                var result = clientHttp.GetAsync(uri).Result;
                //response
                int statusCode = (int)result.StatusCode;
                var body = result.Content.ReadAsStringAsync();
                //response data
                _response.StatusCode = statusCode;
                _response.Roles = null;

                if (statusCode == 200)
                {
                    _response.Roles = FormatRoles(body.Result);
                }
                
            }
            catch(Exception e)
            {
                _response.StatusCode = 500;
                _response.Roles = null;
            }
            return _response;

        }

        

        public String[] FormatRoles(string roles)
        {
            // format role from TXT
            string substring = roles.Substring(roles.IndexOf('[')+1, roles.IndexOf(']')-2);
            substring = substring.Replace("'", "");
            String[] rolesFormated = substring.Split(',');

            return rolesFormated;
        }

        
    }
}