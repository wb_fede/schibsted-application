﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ClientSite.Models;
using ClientSite.Controllers;
using System.Text;

namespace ClientSite.Helpers
{
    public class AuthorizationFilter :  FilterAttribute, IAuthorizationFilter
    {
        private AuthenticationService authenticationService = new AuthenticationService();
        public string Roles { get; set; }
        public string Login { get; set; }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            // get data from action login
            try
            {
                var user = filterContext.HttpContext.Session["username"];
                var pass = filterContext.HttpContext.Session["password"];
                // get roles
                ResponseModel response = authenticationService.CheckAuthentication(user.ToString(), pass.ToString());

                // if not authentication
                if (response.StatusCode != 200)
                {

                    filterContext.Result = new RedirectToRouteResult(
                                                                                        new RouteValueDictionary(new { controller = "Account", action = "login" })
                                                                                    );
                    filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);

                    return;
                }
                
                //check role and site
                if (Roles != null && Array.IndexOf(response.Roles, "Admin") < 0 && Array.IndexOf(response.Roles, Roles) < 0)
                {
                    //query string
                    filterContext.Result = new RedirectToRouteResult(
                                                                    new RouteValueDictionary(new { controller = "Pages", action = "index" })
                                                                );
                    filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
                    return;
                }
                //check redirect
                filterContext.Controller.ViewData.Add("Roles", response.Roles);
            }
            catch(Exception e)
            {
                // generate redirect
                // get controller
                string controllerName = filterContext.RouteData.Values["controller"].ToString();
                // try access any page
                if (controllerName == "Pages")
                {
                    // get action
                    string actionName = filterContext.RouteData.Values["action"].ToString();
                    filterContext.Result = new RedirectToRouteResult(
                                                                new RouteValueDictionary{
                                                                                                { "action", "login" },
                                                                                                { "controller", "Account" },
                                                                                                { "returnUrl", actionName }
                                                                                        });
                }
                else
                {
                    filterContext.Result = new RedirectToRouteResult(
                                                                                        new RouteValueDictionary(new { controller = "Account", action = "login" })
                                                                                    );
                }
                filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
                return;
            }

            
        }

        public string EncodeSession(string value)
        {
            // encode session data
            var byteArray = Encoding.ASCII.GetBytes(value);
            string result = Convert.ToBase64String(byteArray);

            return result;

        }

        public string DecodeSession(string value)
        {
            byte[] byteArray = Convert.FromBase64String(value);
            string result = Encoding.UTF8.GetString(byteArray);

            // base 64 again
            byteArray = Convert.FromBase64String(result);
            result = Encoding.UTF8.GetString(byteArray);

            return result;

        }

    }


}