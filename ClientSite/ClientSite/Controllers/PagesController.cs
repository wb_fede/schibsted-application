﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientSite.Helpers;
using ClientSite.Models;

namespace ClientSite.Controllers
{
    public class PagesController : Controller
    {
        private AuthenticationService authenticationService = new AuthenticationService();

        [AuthorizationFilter]
        public ActionResult Index()
        {
            // get user from session
            string user;
            try
            {
                user = Session["username"].ToString();
            }catch(Exception e)
            {
                return LogOut();
            }
            

            if(user == null)
            {
                return LogOut();
            }
            ViewData["username"] = user;

            //get roles from filter
            ResponseModel response = authenticationService.CheckAuthentication(Session["username"].ToString(), Session["password"].ToString());
            ViewData["roles"] = response.Roles;

            return View();
        }
        [AuthorizationFilter(Roles = "Page1")]
        public ActionResult Page1()
        {
            // get user from session
            string user;
            try
            {
                user = Session["username"].ToString();
            }
            catch (Exception e)
            {
                return LogOut();
            }
            ViewData["username"] = user;
            return View();
        }
        [AuthorizationFilter(Roles = "Page2")]
        public ActionResult Page2()
        {
            // get user from session
            string user;
            try
            {
                user = Session["username"].ToString();
            }
            catch (Exception e)
            {
                return LogOut();
            }
            ViewData["username"] = user;
            return View();
        }
        [AuthorizationFilter(Roles = "Page3")]
        public ActionResult Page3()
        {
            // get user session
            string user;
            try
            {
                user = Session["username"].ToString();
            }
            catch (Exception e)
            {
                return LogOut();
            }
            ViewData["username"] = user;
            return View();
        }

        //[AuthorizationFilter]
        public ActionResult LogOut()
        {
            //destroy session
            if(Session != null)
            {
                Session.Abandon();
            }
            return  RedirectToAction("Login", "Account");
        }
    }
}