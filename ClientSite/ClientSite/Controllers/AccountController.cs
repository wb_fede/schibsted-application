﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ClientSite.Models;
using ClientSite.Helpers;

namespace ClientSite.Controllers
{
    public class AccountController : Controller
    {
        private AuthenticationService authenticationService = new AuthenticationService();
        public string basicAuth;

        //
        // GET: /Account/Login
        public ActionResult Login(string returnUrl)
        {
            //loggin
            if(Session != null && Session["username"] != null)
            {
                return RedirectToAction("index", "Pages");
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            basicAuth = model.Usuario + ':' + model.Password;
            //check authentication
            ResponseModel response = authenticationService.CheckAuthentication(model.Usuario, model.Password);
            if (response.StatusCode != 200)
            {
                ViewData["error"] = true;
                ViewData["msg"] = "Usuario o contraseña incorrecta";
                return View(model);
            }
            //get session data
            Session["username"] = model.Usuario;
            Session["password"] = model.Password;

            //redirect ?
            if(returnUrl != null)
            {
                TempData["redirect"] = returnUrl;
                return RedirectToAction(returnUrl, "Pages");
            }
            else
            {
                return RedirectToAction("index", "Pages");
            }
            

        }

    }
}