﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClientSite.Startup))]
namespace ClientSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
           
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSession();
        }
    }
}
