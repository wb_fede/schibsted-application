﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using ApiRestSchibsted.Helpers;

namespace ApiRestSchibsted.Models
{
    public class User : IValidatableObject
    {
        [Required]
        [MinLength(5)]
        [StringLength(30)]
        public string UserName { get; set; }

        [Required]
        [MinLength(5)]
        [StringLength(10)]
        public string Password { private get; set; }
        // Encrypt this.Password = Helpers.Helpers.Encrypt(value);

        [Required]
        public string Roles { get; set; }

        //Validate ROLES
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var message = "";
            try
            {
                // must to be an array
                String[] obj = new[] { Roles };
                // must have 1 element
                if (obj.Count() < 0)
                {
                    message = "Roles debe tener al menos un elemento";
                }
            }
            catch (Exception e)
            {
                message = "Roles debe ser un array válido";
            }

            //get error
            if (message != "")
            {
                yield return new ValidationResult
                (message, new[] { "Roles" });
            }

        }

        public bool CheckPassword(string p)
        {
            // check if password is correct
            string pass = this.Password;

            if(pass == p)
            {
                return true;
            }
            return false;

        }

        public string GetPassword()
        {
            // get password to write in file
            string pass = this.Password;
            return pass;
        }
    }
}
