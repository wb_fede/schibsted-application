﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Runtime.Caching;
using System.Collections;
using System.Data.OleDb;
using System.Data;
using System.Text;
using System.Data.Common;
using Newtonsoft.Json;
using System.IO;
using System.Web;

namespace ApiRestSchibsted.Models
{
    public class UserContext
    {
        MemoryCache memoryCache = MemoryCache.Default;
        static string path = HttpContext.Current.Server.MapPath("~/App_Data/usuarios.txt");
        string[] lines = System.IO.File.ReadAllLines(path);
        List<User> listaUsuarios = new List<User> { };

        public object Get()
        {

            foreach (string line in lines)
            {
                User user = new User();

                string[] datos = line.Split('|');
                if (datos.Count() > 1)
                {
                    user.UserName = datos[0];
                    user.Password = datos[1];
                    user.Roles = datos[2];
                }

                listaUsuarios.Add(user);
            }

            return listaUsuarios;
        }

        public User GetValue(string key)
        {
            foreach (string line in lines)
            {

                string[] datos = line.Split('|');
                if (datos.Count() > 1)
                {
                    User user = new User();
                    user.UserName = datos[0];
                    user.Password = datos[1];
                    user.Roles = datos[2];

                    if (user.UserName == key)
                    {
                        return user;
                    }
                }
                
            }
            return null;
        }

        public object Add(User user)
        {
            string linea = user.UserName + '|' + user.GetPassword() + '|' + user.Roles;

            //exists?
            object userExist = GetValue(user.UserName);

            if (userExist != null)
            {
                return null;
            }

            //read
            using (StreamWriter w = File.AppendText(path))
            {
                w.WriteLine(linea);
            }

            return user;
        }


        public bool Update(string key, User newUser)
        {
            //read all txt
            string[] lineas = File.ReadAllLines(path);

            for (int i = 0; i < lineas.Count(); i++)
            {
                var linea = lineas[i];
                string[] datos = linea.Split('|');

                if (datos.Count() > 1)
                {
                    User user = new User();
                    user.UserName = datos[0];

                    if (user.UserName == key)
                    {
                        //edit data
                        var lineaText = user.UserName + '|' + newUser.GetPassword() + '|' + newUser.Roles;
                        lineas[i] = lineaText;
                        File.WriteAllLines(path, lineas);
                        return true;
                    }
                }

            }

            return false;

        }


        public void Delete(string key)
        {
            User user = new User();
            user.UserName = key;
            user.Password = "-";
            user.Roles = "-";

            // logic delete
            this.Update(key, user);
        }

    }
}
