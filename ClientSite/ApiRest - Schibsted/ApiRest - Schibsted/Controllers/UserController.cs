﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Security;
using ApiRestSchibsted.Helpers;
using ApiRestSchibsted.Models;

namespace ApiRestSchibsted.Controllers
{
    public class UserController : ApiController
    {

        private UserContext _userContext = new UserContext();

        // GET: api/User
        [AuthenticationAuthorizationFilter(Roles = "Admin")]
        public object Get()
        {
            return _userContext.Get();
        }
        // GET: api/User/5
        [AuthenticationAuthorizationFilter]
        public object Get(string id)
        {
            object username = null;
            //username request must be the same that "id"
            Request.Properties.TryGetValue("Username", out username);

            User user = _userContext.GetValue(id);
            // user found?
            if(username.ToString() != id)
            {
                return BadRequest();
            }
            if (user == null || user.Roles == "-") {
               return NotFound();
            }

            return user.Roles;
        }


        // POST: api/User
        [AuthenticationAuthorizationFilter(Roles = "Admin")]
        public object Post([FromBody]User user)
        {
            if (!ModelState.IsValid) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            //save data
            object userCreated = _userContext.Add(user);
            if (userCreated == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Usuario existente");
            }

            return userCreated;
        }

        // PUT: api/User/5
        [AuthenticationAuthorizationFilter(Roles = "Admin")]
        public object Put(string id, [FromBody]User user)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            //user not found
            if (!_userContext.Update(id, user))
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Not found");
            }
            return Request.CreateErrorResponse(HttpStatusCode.OK, "Usuario actualizado correctamente");

        }

        // DELETE: api/User/5
        [AuthenticationAuthorizationFilter(Roles = "Admin")]
        public object Delete(string id)
        {
            _userContext.Delete(id);
            return Request.CreateErrorResponse(HttpStatusCode.OK, "Usuario eliminado correctamente");
        }

    }
}
