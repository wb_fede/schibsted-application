﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace ApiRestSchibsted.Helpers
{
    public class Helpers
    {
        // encoding
        public static string Encrypt(string strData)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(strData);
            byte[] inArray = HashAlgorithm.Create("SHA1").ComputeHash(bytes);
            return Convert.ToBase64String(inArray);
        }
    }
}