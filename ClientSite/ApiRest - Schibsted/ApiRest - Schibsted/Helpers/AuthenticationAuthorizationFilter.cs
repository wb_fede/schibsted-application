﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using ApiRestSchibsted.Models;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using System.Net.Http.Headers;
using System.Net.Http;


namespace ApiRestSchibsted.Helpers
{
    public class AuthenticationAuthorizationFilter : ActionFilterAttribute
    {
        // get access to data
        private UserContext _userContext = new UserContext();
        private string _authenticationToken;

        public string Roles { get; set; }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            string authenticationToken;
            if (actionContext.Request.Headers.Authorization == null)
            {
                actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                return;
            }
            authenticationToken = actionContext.Request.Headers.Authorization.Parameter;

            // generate string
            string decodedToken = Encoding.UTF8.GetString(Convert.FromBase64String(authenticationToken));
            string username = decodedToken.Substring(0, decodedToken.IndexOf(":"));
            string password = decodedToken.Substring(decodedToken.IndexOf(":")+1);

            //save token
            _authenticationToken = authenticationToken;
            // get user
            User user = (User)_userContext.GetValue(username);
            //check password
            if (user == null || !user.CheckPassword(password))
            {
                actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                return;
            }
            //Authentication ok

            // data to controller
            actionContext.Request.Properties.Add(new KeyValuePair<string, object>("Username", username));

            // Authorization
            if (Roles != null)
            {
                //to array
                //quit [] and ''
                string substring = user.Roles.Substring(1, user.Roles.Length - 2);
                substring = substring.Replace("'", "");
                String[] roles = substring.Split(',');

                //check role
                bool has = Array.IndexOf(roles, Roles) >= 0;
                if (!has)
                {
                    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                }
            }

        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
                
        }
    }
}